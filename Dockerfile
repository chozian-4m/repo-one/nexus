ARG BASE_REGISTRY=registry1.dso.mil
ARG BASE_IMAGE=ironbank/redhat/openjdk/openjdk8
ARG BASE_TAG=latest

FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG}

# ARG NEXUS_VERSION=3.34.1-01
ARG NEXUS_VERSION=3.38.1-01

ENV SONATYPE_DIR=/opt/sonatype
ENV NEXUS_HOME=${SONATYPE_DIR}/nexus \
    NEXUS_DATA=/nexus-data \
    NEXUS_CONTEXT='' \
    SONATYPE_WORK=${SONATYPE_DIR}/sonatype-work \
    USER_NAME=nexus \
    USER_UID=200

COPY ./scripts/uid_entrypoint nexus-unix.tar.gz ./scripts/start-nexus-repo.sh  /

USER 0

RUN yum update -y && \
    yum install tar findutils --nodocs && \
    yum clean all && \
    rm -rf /var/cache/yum && \
    rm -rf /usr/bin/chage /usr/bin/gpasswd /usr/bin/mount /usr/bin/newgrp /usr/bin/passwd /usr/bin/su /usr/bin/sudo /usr/bin/umount /usr/bin/write /usr/libexec/dbus-1/dbus-daemon-launch-helper /usr/libexec/utempter/utempter  /usr/sbin/pam_timestamp_check /usr/sbin/unix_chkpwd  /usr/sbin/userhelper

RUN mkdir -p ${NEXUS_HOME}  \
&& tar -xz -C ${NEXUS_HOME} --strip-components=1 nexus-${NEXUS_VERSION} -f nexus-unix.tar.gz --exclude '*.jar' \
# && tar -xz -C ${NEXUS_HOME} --strip-components=1 nexus-${NEXUS_VERSION} -f nexus-unix.tar.gz \
&& chown -R root:root ${NEXUS_HOME} \
&& sed -e '/^nexus-context/ s:$:${NEXUS_CONTEXT}:' -i ${NEXUS_HOME}/etc/nexus-default.properties \
&& useradd -l -u ${USER_UID} -r -g 0 -m -d ${NEXUS_DATA} -s /sbin/no-login -c "${USER_NAME} application user" ${USER_NAME} \
&& mkdir -p -m 0755 ${NEXUS_HOME}/lib/ && mkdir -p -m 0755 ${NEXUS_HOME}/replicator/ \ 
&& mkdir -p ${NEXUS_DATA}/etc ${NEXUS_DATA}/log ${NEXUS_DATA}/tmp ${SONATYPE_WORK} \
&& ln -s ${NEXUS_DATA} ${SONATYPE_WORK}/nexus3  \
&& chown -R ${USER_NAME}:0 ${NEXUS_DATA} \
&& chown -R ${USER_NAME}:0 ${NEXUS_HOME}/lib && chown -R ${USER_NAME}:0 ${NEXUS_HOME}/system \
&& chown -R ${USER_NAME}:0 ${NEXUS_HOME}/replicator && chown -R ${USER_NAME}:0 ${NEXUS_HOME}/.install4j \
&& chown -R ${USER_NAME}:0 /start-nexus-repo.sh \
&& chmod ug+x /uid_entrypoint \
&& find ${NEXUS_DATA} -type d -exec chmod g+x {} + 
            
VOLUME ${NEXUS_DATA}

USER nexus
WORKDIR ${NEXUS_HOME}

ENV INSTALL4J_ADD_VM_PARAMS="-Xms2703m -Xmx2703m -XX:MaxDirectMemorySize=2703m -Djava.util.prefs.userRoot=${NEXUS_DATA}/javaprefs"

EXPOSE 8081
HEALTHCHECK CMD curl --fail http://localhost:8081/service/rest/v1/status || exit 1
CMD ["sh", "-c", "/start-nexus-repo.sh"]
