## Only approved for use behind CNAP/VPN or on a private network.

<!--

  Copyright (c) 2016-present Sonatype, Inc.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

-->

# Sonatype Nexus Repository OSS & PRO

### Manage binaries and build artifacts across the entire SDLC

* [Nexus Repository OSS](https://www.sonatype.com/products/repository-oss)
* [Nexus Repository PRO](https://www.sonatype.com/products/repository-pro)

 > [Sonatype products do not use log4j-core](https://help.sonatype.com/docs/important-announcements/sonatype-product-log4j-vulnerability-status). This means our software, including Nexus Lifecycle, Nexus Firewall, Nexus Repository Manager OSS, and Nexus Repository Manager Pro in versions 2.x and 3.x, is NOT affected by [CVE-2021-44228](https://ossindex.sonatype.org/vulnerability/f0ac54b6-9b81-45bb-99a4-e6cb54749f9d). We still advise keeping your software upgraded at the latest version.

Sonatype products can help you to find and remediate the log4j vulnerability. See our [Find & Fix Log4j guide](https://help.sonatype.com/docs/important-announcements/find-and-fix-log4j?utm_source=help&utm_medium=email&utm_campaign=log4j-guide).

  > A paid license is necessary to upgrade Nexus Repository Manager OSS to Nexus Repository Manager Pro, and to keep Nexus Repository Manager Pro paid features operational.



The need for organizations to deliver technologies faster is forcing developers to embrace the power of open source development. **Today, almost 15,000 new or updated open source releases are being made available to developers every day.**

The flow of these components into and through an organization creates a complex software supply chain that can negatively impact speed, efficiency, and quality if not properly managed.

Nexus Repository Manager serves as the universal local warehouse to efficiently manage and distribute component parts, assemblies, and finished goods across your entire software supply chain. 


* [Running with Persistent Data](#running-with-persistent-data)
* [Notes](#notes)
* [Getting Help](#getting-help)

### Running with Persistent Data

There are two general approaches to handling persistent storage requirements
with Docker. See [Managing Data in Containers](https://docs.docker.com/engine/tutorials/dockervolumes/)
for additional information.

  1. *Use a docker volume*.  Since docker volumes are persistent, a volume can be created specifically for
  this purpose.  This is the recommended approach.  

  ```
  $ docker volume create --name nexus-data
  $ docker run -d -p 8081:8081 --name nexus -v nexus-data:/nexus-data registry1.dso.mil/ironbank/sonatype/nexus/nexus:latest
  ```

  2. *Mount a host directory as the volume*.  This is not portable, as it
  relies on the directory existing with correct permissions on the host.
  However it can be useful in certain situations where this volume needs
  to be assigned to certain specific underlying storage.  

  ```
  $ mkdir /some/dir/nexus-data && chown -R 200 /some/dir/nexus-data
  $ docker run -d -p 8081:8081 --name nexus -v /some/dir/nexus-data:/nexus-data registry1.dso.mil/ironbank/sonatype/nexus/nexus:latest
  ```

To test:

```
$ curl -u admin:admin123 http://<docker hostname>:8081/service/metrics/ping
```

## Deployment Example Chart
There is an example change located [here](https://repo1.dsop.io/dsop/charts/-/tree/master/stable/nexus-repository-manager). Some settings may have to be modified such as:
* hostname/ip
* hostAliases
* securityContext (privs)
* storageClass (based on what is available in the target environment)
* etc...

## Notes

* Default credentials are: `admin` / `admin123`

* It can take some time (2-3 minutes) for the service to launch in a
new container.  You can tail the log to determine once Nexus is ready:

```
$ docker logs -f nexus
```

* Installation of Nexus is to `/opt/sonatype/nexus`.  

* A persistent directory, `/nexus-data`, is used for configuration,
logs, and storage. This directory needs to be writable by the Nexus
process, which runs as UID 200.

* There is an environment variable that is being used to pass JVM arguments to the startup script

  * `INSTALL4J_ADD_VM_PARAMS`, passed to the Install4J startup script. Defaults to `-Xms1200m -Xmx1200m -XX:MaxDirectMemorySize=2g -Djava.util.prefs.userRoot=${NEXUS_DATA}/javaprefs`.

  This can be adjusted at runtime:

  ```
  $ docker run -d -p 8081:8081 --name nexus -e INSTALL4J_ADD_VM_PARAMS="-Xms2g -Xmx2g -XX:MaxDirectMemorySize=3g  -Djava.util.prefs.userRoot=/some-other-dir" registry1.dso.mil/ironbank/sonatype/nexus/nexus:latest
  ```

  Of particular note, `-Djava.util.prefs.userRoot=/some-other-dir` can be set to a persistent path, which will maintain
  the installed Nexus Repository License if the container is restarted.

* Another environment variable can be used to control the Nexus Context Path

  * `NEXUS_CONTEXT`, defaults to /

  This can be supplied at runtime:

  ```
  $ docker run -d -p 8081:8081 --name nexus -e NEXUS_CONTEXT=nexus registry1.dso.mil/ironbank/sonatype/nexus/nexus:latest
  ```

## Getting Help

Looking to contribute to our Docker image but need some help? There's a few ways to get information or our attention:

* Chat with us on [Gitter](https://gitter.im/sonatype/nexus-developers)
* File an issue [on our public JIRA](https://issues.sonatype.org/projects/NEXUS/)
* Check out the [Nexus3](http://stackoverflow.com/questions/tagged/nexus3) tag on Stack Overflow
* Check out the [Nexus Repository User List](https://groups.google.com/a/glists.sonatype.com/forum/?hl=en#!forum/nexus-users)

