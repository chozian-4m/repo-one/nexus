#!/usr/bin/bash
#
# Copyright:: Copyright (c) 2017-present Sonatype, Inc. Apache License, Version 2.0.
#

NEXUS_HOME=/opt/sonatype/nexus

tar xvfz /nexus-unix.tar.gz --strip-components=1 -C ${NEXUS_HOME} --wildcards *.jar
echo "Starting Nexus..."
${NEXUS_HOME}/bin/nexus run
